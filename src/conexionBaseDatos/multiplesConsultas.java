/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexionBaseDatos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.table.DefaultTableModel;


public class multiplesConsultas extends conexionBaseDatos{
  
  public multiplesConsultas ( Connection _conexion){
    conexion = _conexion;
  }

  @Override
  public boolean ingresar(Map<String, Object> values) throws SQLException {   
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public boolean borrar(int id_fila) throws SQLException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public boolean actualizar(int id_fila, Map<String, Object> values) throws SQLException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public DefaultTableModel selecionarLista() throws SQLException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public List<Object> Seleccion(int id_fila) throws SQLException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }
  
  public boolean ingresar_multi(List <Object>  values, String nombre_tabla,String selecion) throws SQLException {
    
    String valores = "";
    int contador = 0;
    
    
    for (int i = 0; i < values.size() ; i++) {
      System.out.println( i );
      if( i == 0){
        valores = "?";
      }else{
        valores = valores + ",?";
      }   
    }
    
    String sql_query = ("INSERT INTO " + nombre_tabla + " (" + selecion + ") VALUES " + "(" + valores + ")"
             
    );
    

    PreparedStatement declaracion = conexion.prepareStatement( sql_query );
     System.out.println( declaracion );

  for (int i = 0; i <= values.size() - 1; i++) {
    contador = i + 1;
    declaracion.setObject(contador, values.get( i ));

  }

    int filas_agregadas = declaracion.executeUpdate();
    declaracion.close();
    return filas_agregadas > 0;
    
  }
  
  public DefaultTableModel selecionarListaMulti(String nombreTabla,String selecion )throws SQLException {
   

    
    String sql_query = (
      "SELECT " + 
      selecion + " " +
      "FROM " + nombreTabla
    );
    
    PreparedStatement declaracion = conexion.prepareStatement(sql_query);
    ResultSet resultado = declaracion.executeQuery();
    
    //Pasar de ResultSet a DefaultTableModel
    DefaultTableModel modelo = new DefaultTableModel()
    {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    utilidades.ConversorResultSetADefaultTableModel.rellena(resultado, modelo );
    //
    declaracion.close();
    resultado.close();
    return modelo;
    
  }
  
  public  List<Object> SeleccionObjeto(int id_fila,String nombreTabla, String id_pk, String seleccion) throws SQLException {
    
    
    String sql_query = ( 
            
      "SELECT " + 
      seleccion + " " +
      "FROM " + nombreTabla + " "+
      "WHERE " + id_pk + " = ?"
            
    );
    
    PreparedStatement declaracion = conexion.prepareStatement(sql_query);
    declaracion.setObject(1, id_fila);
    System.out.println(declaracion);
    ResultSet resultado = declaracion.executeQuery();
    ResultSetMetaData md = resultado.getMetaData();
    int columnas = md.getColumnCount();
    
    List<Object> fila_lista = new ArrayList<>();
    while( resultado.next() ){
      
      Map < String, Object > fila = new HashMap< String, Object >();
      for( int i = 1; i <= columnas; i++){
        
        fila.put(md.getColumnLabel(i).toLowerCase(), resultado.getObject(i));
     
      }
      
      fila_lista.add(fila);
      
    }
    
    declaracion.close();
    resultado.close();
    return fila_lista;
    
  }
  
  public boolean Elimnar(int id_fila, String nombreTabla, String id_pk) throws SQLException {
    
    
    String sql_query = ( "DELETE " + "FROM " + nombreTabla + " WHERE " + id_pk + " = ?" );
    
    PreparedStatement declaracion = conexion.prepareStatement(sql_query);
    declaracion.setObject(1, id_fila);
    
    System.out.println(declaracion);
    
    int fila_eliminada = declaracion.executeUpdate();
    declaracion.close();
    
    return fila_eliminada > 0;
    
  }
  
  public boolean actualizar_datos( List< Object > values, 
                                  String nombre_tabla, String id_pk, String columnas_valores ) throws SQLException {
    
    int contador = 0;
    
    String sql_query = (
      "UPDATE " + 
      nombre_tabla + " " +
      "SET " + columnas_valores + " " +
      "WHERE " + id_pk + " = ?" 
    );
    
    System.out.println(sql_query);
    
    PreparedStatement declaracion = conexion.prepareStatement(sql_query);
    for (int i = 0; i <= values.size() - 1; i++) {
      contador = i + 1;
      declaracion.setObject(contador, values.get( i ));
    }
    System.out.println(declaracion);

    int filas_actualizadas = declaracion.executeUpdate();
    declaracion.close();
    return filas_actualizadas > 0;
    
  }
  
  
  public DefaultTableModel busqueda ( String tabla , String campo , String busqueda, 
                                      String seleccionar, String complemento )throws SQLException
  {
      
    String sql_query = (
      "SELECT " + 
      seleccionar + " " +
      "FROM " + tabla + " WHERE " + campo + " like '"+ busqueda + "%'" + complemento
      );
    
    PreparedStatement declaracion = conexion.prepareStatement(sql_query);
    ResultSet resultado = declaracion.executeQuery();
    
    //Pasar de ResultSet a DefaultTableModel
    DefaultTableModel modelo = new DefaultTableModel()
    {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    utilidades.ConversorResultSetADefaultTableModel.rellena(resultado, modelo );
    //
    declaracion.close();
    resultado.close();
    return modelo;
  }
  
  
  
 
  
}
