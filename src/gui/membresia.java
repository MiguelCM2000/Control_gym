/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import conexionBaseDatos.consultaMembresiaDatos;
import conexionBaseDatos.multiplesConsultas;
import java.awt.Font;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import utilidades.TextPrompt;
import utilidades.configuracionXml;
import utilidades.notificaciones;

/**
 *
 * @author L
 */
public class membresia extends javax.swing.JInternalFrame {
  
  configuracionXml con = new configuracionXml();
  multiplesConsultas conexion = new multiplesConsultas(con.getConexion().getConexion());
  notificaciones notificaciones = new notificaciones();
  
  String nombre_tabla = "membresia_datos";
  String id_tabla = "id_menbresia_datos";
  String valores = "descripcion, precio, nombre, dias";
  String valores_actualizar = "descripcion = ?, precio = ?, nombre = ? , dias = ?";
  int seleccion_id = 0;
  
  public membresia() {
    initComponents();
    selecionarlista_membresia();
    configurar_frame();
  }
  
  
  private void configurar_frame(){
    
    
    _dias_etiqueta.setVisible(true);
    _precio_etiqueta.setVisible(false);
    _nombre_etiqueta.setVisible(false);
    _descripcion_etiqueta.setVisible(false);
    jPanel7.setVisible(true);
    
    TextPrompt placeholder_precio = new TextPrompt("Precio", precio);
      placeholder_precio.changeAlpha(0.75f);
      placeholder_precio.changeStyle(Font.BOLD);
      
    TextPrompt placeholder_nombre = new TextPrompt("Nombre", nombre);
      placeholder_nombre.changeAlpha(0.75f);
      placeholder_nombre.changeStyle(Font.BOLD);
      
   TextPrompt placeholder_descripcion = new TextPrompt( "Descripcion" , descripcion);
      placeholder_descripcion.changeAlpha(0.75f);
      placeholder_descripcion.changeStyle(Font.BOLD);
      
   TextPrompt placeholder_busqueda = new TextPrompt( "Busqueda" , busqueda);
      placeholder_busqueda.changeAlpha(0.75f);
      placeholder_busqueda.changeStyle(Font.BOLD);
      
    llenar_comobo_busqueda();
    
  }
  
  
  private void llenar_comobo_busqueda(){
    jComboBusqueda.removeAllItems();
    jComboBusqueda.addItem("Seleccionar campo");
    jComboBusqueda.addItem("id_menbresia_datos");
    jComboBusqueda.addItem("descripcion");
    jComboBusqueda.addItem("precio");
    jComboBusqueda.addItem("nombre");
    jComboBusqueda.addItem("dias");
    
  } 
  
  
  private void limpiar(){
    
    noDias.setValue(0);
    precio.setText("");
    nombre.setText("");
    descripcion.setText("");
    
  }
  
  
  private void guardar(){
    
    String _nombre = nombre.getText();
    String _precio = precio.getText();
    int _noDias = (int) noDias.getValue();
    String _decripcion = descripcion.getText(); 
    
    try {
      
      if( !_nombre.equals("") && !_precio.equals("") && _noDias != 0 && !_decripcion.equals("")){
       
        List <Object> datos_1 = new ArrayList<Object>();
          datos_1.add(_decripcion);
          datos_1.add(_precio);
          datos_1.add(_nombre);
          datos_1.add(String.valueOf(_noDias));
          
      boolean resultado = conexion.ingresar_multi(datos_1,nombre_tabla ,valores);
      
      if(resultado){
        
        notificaciones.mensajeCorrecto("Los datos de membresia " +_nombre+ " se a guardado");
        limpiar();
        selecionarlista_membresia();
        
      }else{
        notificaciones.mensajeError("La membresia no se logro guardar");
      }
      
    }else{
        notificaciones.mensajeAdvertencia("Porfavor revise todos los campos del formulario");
    }
      
    } catch (Exception e) {
      
      notificaciones.mensajeError( "Fallo el guardado de los datos de membresia" );
      
    }
    
  }
  
  private void selecionarlista_membresia(){
    
    try {
      
      String seleccion = "*";
      DefaultTableModel modeloMembresia = new DefaultTableModel();
      modeloMembresia = conexion.selecionarListaMulti(nombre_tabla, seleccion);
      jTableMembresia.setModel(modeloMembresia);
    } catch (SQLException ex) {
      System.out.print(ex);
      Logger.getLogger(membresia.class.getName()).log(Level.SEVERE, null, ex);
    }
    
  }
  
  private void selecionarListaClientes(){
    
    String seleccion = "*";
    panelPrincipal.setSelectedIndex(0);
    btnGuardar.setText("Guardar cambios");
    
    
    int fila = jTableMembresia.getSelectedRow();
    
    if (fila >= 0 ) {
      
      try {
        DefaultTableModel modeloMembresia = (DefaultTableModel) jTableMembresia.getModel();
        int id_fila = Integer.parseInt( modeloMembresia.getValueAt(fila, 0 ).toString());
        seleccion_id = id_fila;
        List< Object > membresias = conexion.SeleccionObjeto(id_fila, nombre_tabla, id_tabla, seleccion);
        
        if( membresias.size() > 0){
          
          Map<String, Object> map = ( ( Map<String, Object> ) ( membresias.get( 0 ) ) );
          String _nombre = map.get("nombre").toString();
          String _precio= map.get("precio").toString();
          String _dias = map.get("dias").toString();
          String _descripcion = map.get("descripcion").toString();
          
          noDias.setValue( Integer.parseInt(_dias) );
          precio.setText(_precio);
          nombre.setText(_nombre);
          descripcion.setText(_descripcion);
          
        }else{
          notificaciones.mensajeError( "No se lograron recuperar los datos" );
        }
        
        
      } catch (SQLException ex) {
        
        notificaciones.mensajeError( "Fallo la seleccion del objeto" );
        System.out.println(ex);
        Logger.getLogger(membresia.class.getName()).log(Level.SEVERE, null, ex);
      }
      
    }
  } 
  
  
  private void eliminar(){
    
    int fila = jTableMembresia.getSelectedRow();
    
    if( fila >= 0){
      
      try {
        
        DefaultTableModel modeloMembresia = (DefaultTableModel) jTableMembresia.getModel();
        int id_fila = Integer.parseInt( modeloMembresia.getValueAt(fila, 0 ).toString());
        boolean resultado = conexion.Elimnar(id_fila, nombre_tabla, id_tabla);
        
        if(resultado){
          
          selecionarlista_membresia();
          notificaciones.mensajeCorrecto( "Se ha eliminado la membresia " );
          
        }else{
          
          notificaciones.mensajeError("No se logro eliminar la membresia");
          
        }
        
      } catch (SQLException ex) {
        notificaciones.mensajeError("la accion eliminar dejo de funcionar");
        System.out.println(ex);
        Logger.getLogger(membresia.class.getName()).log(Level.SEVERE, null, ex);
      }
      
      
    }
    
  }
  
  private void confirmar_eliminacion(){
    
    
    int dialogo = JOptionPane.YES_NO_OPTION;
    int respuesta = JOptionPane.showConfirmDialog(null, " ¿ Estas seguro de esta accion ?", " Eliminar ", dialogo);
    
    if(respuesta == 0){
      eliminar();
    }
    
    
  }
  
  
  private void opcion_boton_guardar(){
    
    if( btnGuardar.getText().equals("Guardar") ){
      guardar();
    }else{
      actualizar();
    }
    
  }
  
  
  private void actualizar(){
    
    String _nombre = nombre.getText();
    String _precio = precio.getText();
    int _noDias = (int) noDias.getValue();
    String _descripcion = descripcion.getText(); 
    
    try {
      
      if( (!_nombre.equals("")) && ( !_precio.equals("") ) && ( _noDias != 0 ) && ( !_descripcion.equals("") )  ){
        
        List < Object > values = new ArrayList<Object>();
          values.add(_descripcion);
          values.add(_precio);
          values.add(_nombre);
          values.add(String.valueOf(_noDias));
          values.add( seleccion_id );
          
          
          
       //comparamos el numero de la fila 
        int fila = jTableMembresia.getSelectedRow();
        
        if ( fila >= 0 ) 
        {
          boolean resultado = conexion.actualizar_datos(values, nombre_tabla, id_tabla, valores_actualizar);
          //comparmaos que el valor no este vacio 
          if( resultado )
          {
            
            limpiar();
            btnGuardar.setText("Guardar");
            selecionarlista_membresia();
            notificaciones.mensajeCorrecto( "Se acualizaron los datos correctamente" );

          }
          else
          {
            notificaciones.mensajeError("no se lograon guardar los datos");
          }
          
        }
        
        
      }else{
        notificaciones.mensajeAdvertencia( "Porfavor verifique que los datos este completos" );
      }
      
    } catch (Exception e) {
      
      notificaciones.mensajeError("No se logro realizar la accion");
      System.out.print(e);
    }
    
  }
  
  
  private void busqueda(){
    
    
    try {
      
    String campo = jComboBusqueda.getSelectedItem().toString();
    String _busqueda = busqueda.getText();
    String seleccionar = "*", complemento = ""; 
      
    if( (!campo.equals("Seleccionar campo")) ||
        (!_busqueda.equals(""))){
      
      
     
        DefaultTableModel modelo_tabla_membresia = new DefaultTableModel();
        modelo_tabla_membresia = conexion.busqueda(nombre_tabla,campo,_busqueda, seleccionar, complemento);
        jTableMembresia.setModel( modelo_tabla_membresia );
        
      
        
      
    }else{
       notificaciones.mensjeInformacion("Selecione un campo o coloque su busqueda");
    }
    } catch (SQLException ex) {
      
      
        Logger.getLogger(membresia.class.getName()).log(Level.SEVERE, null, ex);
      
      }
    
  }

 
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    menuOpciones_click_derecho = new javax.swing.JPopupMenu();
    jMenuItemEditar = new javax.swing.JMenuItem();
    jMenuItemEliminar = new javax.swing.JMenuItem();
    jPanel1 = new javax.swing.JPanel();
    panelPrincipal = new javax.swing.JTabbedPane();
    jPanel2 = new javax.swing.JPanel();
    _nombre_etiqueta = new javax.swing.JLabel();
    nombre = new javax.swing.JTextField();
    _precio_etiqueta = new javax.swing.JLabel();
    precio = new javax.swing.JTextField();
    noDias = new javax.swing.JSpinner();
    _dias_etiqueta = new javax.swing.JLabel();
    _descripcion_etiqueta = new javax.swing.JLabel();
    jScrollPane1 = new javax.swing.JScrollPane();
    descripcion = new javax.swing.JTextArea();
    btnGuardar = new javax.swing.JButton();
    btnLimpiar = new javax.swing.JButton();
    jPanel4 = new javax.swing.JPanel();
    jPanel5 = new javax.swing.JPanel();
    jPanel6 = new javax.swing.JPanel();
    jPanel3 = new javax.swing.JPanel();
    jScrollPane2 = new javax.swing.JScrollPane();
    jTableMembresia = new javax.swing.JTable();
    jComboBusqueda = new javax.swing.JComboBox();
    busqueda = new javax.swing.JTextField();
    jPanel7 = new javax.swing.JPanel();
    jButton3 = new javax.swing.JButton();

    menuOpciones_click_derecho.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N

    jMenuItemEditar.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
    jMenuItemEditar.setText("Editar");
    jMenuItemEditar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jMenuItemEditarActionPerformed(evt);
      }
    });
    menuOpciones_click_derecho.add(jMenuItemEditar);

    jMenuItemEliminar.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
    jMenuItemEliminar.setText("Eliminar");
    jMenuItemEliminar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jMenuItemEliminarActionPerformed(evt);
      }
    });
    menuOpciones_click_derecho.add(jMenuItemEliminar);

    jPanel1.setBackground(new java.awt.Color(30, 30, 30));

    jPanel2.setBackground(new java.awt.Color(30, 30, 30));

    _nombre_etiqueta.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    _nombre_etiqueta.setForeground(new java.awt.Color(255, 255, 255));
    _nombre_etiqueta.setText("Nombre");

    nombre.setBackground(new java.awt.Color(30, 30, 30));
    nombre.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
    nombre.setForeground(new java.awt.Color(255, 255, 255));
    nombre.setBorder(null);
    nombre.setCaretColor(new java.awt.Color(255, 255, 255));

    _precio_etiqueta.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    _precio_etiqueta.setForeground(new java.awt.Color(255, 255, 255));
    _precio_etiqueta.setText("Precio");

    precio.setBackground(new java.awt.Color(30, 30, 30));
    precio.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
    precio.setForeground(new java.awt.Color(255, 255, 255));
    precio.setBorder(null);
    precio.setCaretColor(new java.awt.Color(255, 255, 255));

    _dias_etiqueta.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    _dias_etiqueta.setForeground(new java.awt.Color(255, 255, 255));
    _dias_etiqueta.setText("Dias de duracion");

    _descripcion_etiqueta.setBackground(new java.awt.Color(255, 255, 255));
    _descripcion_etiqueta.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    _descripcion_etiqueta.setForeground(new java.awt.Color(255, 255, 255));
    _descripcion_etiqueta.setText("Descripcion");

    descripcion.setBackground(new java.awt.Color(30, 30, 30));
    descripcion.setColumns(20);
    descripcion.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
    descripcion.setForeground(new java.awt.Color(255, 255, 255));
    descripcion.setLineWrap(true);
    descripcion.setRows(5);
    descripcion.setWrapStyleWord(true);
    descripcion.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
    descripcion.setCaretColor(new java.awt.Color(255, 255, 255));
    jScrollPane1.setViewportView(descripcion);

    btnGuardar.setBackground(new java.awt.Color(249, 133, 42));
    btnGuardar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    btnGuardar.setForeground(new java.awt.Color(255, 255, 255));
    btnGuardar.setText("Guardar");
    btnGuardar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnGuardarActionPerformed(evt);
      }
    });

    btnLimpiar.setBackground(new java.awt.Color(249, 133, 42));
    btnLimpiar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    btnLimpiar.setForeground(new java.awt.Color(255, 255, 255));
    btnLimpiar.setText("Limpiar");
    btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnLimpiarActionPerformed(evt);
      }
    });

    jPanel4.setBackground(new java.awt.Color(249, 133, 42));
    jPanel4.setPreferredSize(new java.awt.Dimension(0, 2));

    javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
    jPanel4.setLayout(jPanel4Layout);
    jPanel4Layout.setHorizontalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel4Layout.setVerticalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    jPanel5.setBackground(new java.awt.Color(249, 133, 42));
    jPanel5.setPreferredSize(new java.awt.Dimension(0, 2));

    javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
    jPanel5.setLayout(jPanel5Layout);
    jPanel5Layout.setHorizontalGroup(
      jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel5Layout.setVerticalGroup(
      jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    jPanel6.setBackground(new java.awt.Color(249, 133, 42));
    jPanel6.setPreferredSize(new java.awt.Dimension(0, 2));

    javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
    jPanel6.setLayout(jPanel6Layout);
    jPanel6Layout.setHorizontalGroup(
      jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel6Layout.setVerticalGroup(
      jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addGap(255, 255, 255)
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(_descripcion_etiqueta))
        .addGap(60, 60, 60)
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addComponent(_dias_etiqueta)
            .addGap(0, 0, Short.MAX_VALUE))
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
              .addComponent(_precio_etiqueta, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(precio)
              .addComponent(_nombre_etiqueta, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(nombre)
              .addComponent(noDias, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
              .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
              .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
              .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
              .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
    );
    jPanel2Layout.setVerticalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addGap(122, 122, 122)
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(_descripcion_etiqueta)
          .addComponent(_dias_etiqueta))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addComponent(noDias, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(26, 26, 26)
            .addComponent(_precio_etiqueta)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(precio, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(14, 14, 14)
            .addComponent(_nombre_etiqueta)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(nombre, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
              .addComponent(btnGuardar)
              .addComponent(btnLimpiar)))
          .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap(111, Short.MAX_VALUE))
    );

    panelPrincipal.addTab("Registro de membresia", jPanel2);

    jPanel3.setBackground(new java.awt.Color(30, 30, 30));

    jTableMembresia.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
    jTableMembresia.setModel(new javax.swing.table.DefaultTableModel(
      new Object [][] {
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null}
      },
      new String [] {
        "Title 1", "Title 2", "Title 3", "Title 4"
      }
    ));
    jTableMembresia.setComponentPopupMenu(menuOpciones_click_derecho);
    jScrollPane2.setViewportView(jTableMembresia);

    jComboBusqueda.setBackground(new java.awt.Color(249, 133, 42));
    jComboBusqueda.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jComboBusqueda.setForeground(new java.awt.Color(255, 255, 255));

    busqueda.setBackground(new java.awt.Color(30, 30, 30));
    busqueda.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
    busqueda.setForeground(new java.awt.Color(255, 255, 255));
    busqueda.setBorder(null);
    busqueda.setCaretColor(new java.awt.Color(255, 255, 255));
    busqueda.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyReleased(java.awt.event.KeyEvent evt) {
        busquedaKeyReleased(evt);
      }
    });

    jPanel7.setBackground(new java.awt.Color(249, 133, 42));
    jPanel7.setPreferredSize(new java.awt.Dimension(0, 3));

    javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
    jPanel7.setLayout(jPanel7Layout);
    jPanel7Layout.setHorizontalGroup(
      jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel7Layout.setVerticalGroup(
      jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 3, Short.MAX_VALUE)
    );

    jButton3.setBackground(new java.awt.Color(249, 133, 42));
    jButton3.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jButton3.setForeground(new java.awt.Color(255, 255, 255));
    jButton3.setText("Buscar");
    jButton3.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton3ActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jScrollPane2)
      .addGroup(jPanel3Layout.createSequentialGroup()
        .addGap(46, 46, 46)
        .addComponent(jComboBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
          .addComponent(busqueda, javax.swing.GroupLayout.DEFAULT_SIZE, 547, Short.MAX_VALUE)
          .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, 547, Short.MAX_VALUE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(37, Short.MAX_VALUE))
    );
    jPanel3Layout.setVerticalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jComboBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addGroup(jPanel3Layout.createSequentialGroup()
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
              .addComponent(jButton3)
              .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(busqueda)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 504, Short.MAX_VALUE))
    );

    panelPrincipal.addTab("Lista de membresia", jPanel3);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(panelPrincipal)
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(panelPrincipal)
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
    // TODO add your handling code here:
    limpiar();
  }//GEN-LAST:event_btnLimpiarActionPerformed

  private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
    // TODO add your handling code here:
    opcion_boton_guardar();
  }//GEN-LAST:event_btnGuardarActionPerformed

  private void jMenuItemEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEditarActionPerformed
    // TODO add your handling code here:
    selecionarListaClientes();
    
  }//GEN-LAST:event_jMenuItemEditarActionPerformed

  private void jMenuItemEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEliminarActionPerformed
    // TODO add your handling code here:
    confirmar_eliminacion();
  }//GEN-LAST:event_jMenuItemEliminarActionPerformed

  private void busquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyReleased
    // TODO add your handling code here:
    busqueda();
  }//GEN-LAST:event_busquedaKeyReleased

  private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
    // TODO add your handling code here:
    busqueda();
  }//GEN-LAST:event_jButton3ActionPerformed


  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JLabel _descripcion_etiqueta;
  private javax.swing.JLabel _dias_etiqueta;
  private javax.swing.JLabel _nombre_etiqueta;
  private javax.swing.JLabel _precio_etiqueta;
  private javax.swing.JButton btnGuardar;
  private javax.swing.JButton btnLimpiar;
  private javax.swing.JTextField busqueda;
  private javax.swing.JTextArea descripcion;
  private javax.swing.JButton jButton3;
  private javax.swing.JComboBox jComboBusqueda;
  private javax.swing.JMenuItem jMenuItemEditar;
  private javax.swing.JMenuItem jMenuItemEliminar;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JPanel jPanel3;
  private javax.swing.JPanel jPanel4;
  private javax.swing.JPanel jPanel5;
  private javax.swing.JPanel jPanel6;
  private javax.swing.JPanel jPanel7;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JTable jTableMembresia;
  private javax.swing.JPopupMenu menuOpciones_click_derecho;
  private javax.swing.JSpinner noDias;
  private javax.swing.JTextField nombre;
  private javax.swing.JTabbedPane panelPrincipal;
  private javax.swing.JTextField precio;
  // End of variables declaration//GEN-END:variables
}
